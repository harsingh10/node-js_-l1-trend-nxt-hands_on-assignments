const fs = require("fs");

const fileName1= process.argv[2]+".txt";
const fileName2 = process.argv[3]+".txt";


var data = '';

// Create a readable stream
var readerStream = fs.createReadStream(fileName1);

// Set the encoding to be utf8.
readerStream.setEncoding('UTF8');

// Handle stream events --> data, end, and error
readerStream.on('data', function(chunk) {
   data += chunk;
});

readerStream.on('end',function() {
   console.log(data);
   writefunction();

});

readerStream.on('error', function(err) {
   console.log(err.stack);
});

console.log("Program Ended");
//writestream


// Create a writable stream
function writefunction(){
  var writerStream = fs.createWriteStream(fileName2);

  // Write the data to stream with encoding to be utf8
  writerStream.write(data,'UTF8');

  // Mark the end of file
  writerStream.end();
  // Handle stream events --> finish, and error
  writerStream.on('finish', function() {
     console.log("Write completed.");
  });

  writerStream.on('error', function(err) {
     console.log(err.stack);
  });

  console.log("Program Ended");

}
