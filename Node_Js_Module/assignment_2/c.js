const events = require('events');

var eventEmitter = new events.EventEmitter();


eventEmitter.on("one", () => {
  console.log('First Event!');
});
eventEmitter.emit('one');
eventEmitter.on("two", () => {
  console.log("Second Event!");

});
eventEmitter.emit('two');
eventEmitter.on("three", ()=>{
  console.log("Third Event!")
});
eventEmitter.emit('three');
