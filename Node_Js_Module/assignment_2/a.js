const http = require("http");

const server = http.createServer((req, res) => {
  // res.writeHead(200, { 'Content-Type': 'text/plain' });
  // res.writeFile("index.html");
  // res.end('okay');
  res.setHeader("Content-Type", "text/html");
  res.writeHead(200);
  res.end(`<html><body><h1>This is HTML</h1></body></html>`);
});

port = 3000 || process.env.PORT;
server.listen(port, () => {
  console.log("your server is up running on =", +port);
});
