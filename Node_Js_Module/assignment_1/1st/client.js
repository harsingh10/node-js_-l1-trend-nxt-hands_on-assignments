const net = require("net");
// const telnet = require('telnet-stream');
// const server = new telnet();
const Telnet = require('telnet-client');
const client = net.createConnection({ port: 9898 }, () => {
  console.log('CLIENT: I connected to the server.');
  client.write('CLIENT: Hello Server!');
});
client.on('data', (data) => {
  console.log(data.toString());
  client.end();
});
// client.on('end', () => {
//   console.log('CLIENT: I disconnected from the server.');
// });


// // display server response
// server.on("data", function(data){
//     console.log(''+data);
// });
//
// // login when connected
// server.on("connect", function(){
//     server.write("login sangwanpankajs@gmail.com 8814");
// });
//
// // connect to server
// server.connect({
//     host: "192.168.0.1",
//     port: 9898
// });


// async function run() {
//   let connection = new Telnet();
//
//   // these parameters are just examples and most probably won't work for your use-case.
//   let params = {
//     host: '198.168.0.1',
//     port: 9898,
//     shellPrompt: '/ # ', // or negotiationMandatory: false
//     timeout: 1500
//   }
//
//   try {
//     await connection.connect(params);
//   } catch(error) {
//
//   }
//
//   let res = await connection.exec('uptime');
//   console.log('async result:', res);
// }
//
// run()
